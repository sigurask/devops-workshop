import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        String ans1 = calculatorResource.calculate(expression);
        assertEquals("400", ans1);

        expression = " 300 - 99 ";
        String ans2 = calculatorResource.calculate(expression);
        assertEquals("201", ans2);

        expression = "42 * 3";
        String ans3 = calculatorResource.calculate(expression);
        assertEquals("126", ans3);

        expression = "28 / 7";
        String ans4 = calculatorResource.calculate(expression);
        assertEquals("4", ans4);
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        String ans1 = Integer.toString(calculatorResource.sum(expression));
        assertEquals("400", ans1);

        expression = "300+99";
        String ans2 = Integer.toString(calculatorResource.sum(expression));
        assertEquals("399", ans2);

        expression = "50+10+100";
        String ans3 = Integer.toString(calculatorResource.sum(expression));
        assertEquals("160", ans3);
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        String ans1 = Integer.toString(calculatorResource.subtraction(expression));
        assertEquals("899", ans1);

        expression = "20-2";
        String ans2 = Integer.toString(calculatorResource.subtraction(expression));
        assertEquals("18", ans2);
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10*10";
        String ans1 = Integer.toString(calculatorResource.multiplication(expression));
        assertEquals("100", ans1);

        expression = "10*7";
        String ans2 = Integer.toString(calculatorResource.multiplication(expression));
        assertEquals("70", ans2);
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/5";
        String ans1 = Integer.toString(calculatorResource.division(expression));
        assertEquals("20", ans1);

        expression = "200/10";
        String ans2 = Integer.toString(calculatorResource.division(expression));
        assertEquals("20", ans2);
    }
}
